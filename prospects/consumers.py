from asgiref.sync import async_to_sync
from channels.generic.websocket import JsonWebsocketConsumer

from .models import TechnicalSkill, Prospect
from .serializers import ProspectSerializer, TechnicalSkillSerializer, ExperienceSerialzer, EducationSerializer, \
    ContactSerializer


class ProspectConsumer(JsonWebsocketConsumer):
    def connect(self):
        self.list_group_name = 'prospects'
        async_to_sync(self.channel_layer.group_add)(
            self.list_group_name,
            self.channel_name
        )

        self.accept()

    def disconnect(self, code):
        async_to_sync(self.channel_layer.group_discard)(
            self.list_group_name,
            self.channel_name
        )

    def receive_json(self, content, **kwargs):
        if 'payload' in content:
            prospect_id = content['stream']
            prospect = Prospect.objects.get(id=prospect_id)

            if content['payload']['action'] == 'retrieve':
                prospect_data = ProspectSerializer(prospect).data
                return self.send_json(content=prospect_data)
            attr = content['payload']['type']

            if attr == 'experiences':
                prospect_data = ExperienceSerialzer(prospect.experiences, many=True).data
            elif attr == 'education':
                prospect_data = EducationSerializer(prospect.education, many=True).data
            elif attr == 'contacts':
                prospect_data = ContactSerializer(prospect.personal_information.contacts, many=True).data
            elif attr == 'technical_skills':
                qs = prospect.technical_skills.all()
                prospect_data = TechnicalSkillSerializer(qs, many=True).data

            else:
                prospect_data = 'No matching query'

            return self.send_json(content=prospect_data)

        async_to_sync(self.channel_layer.group_send)(
            self.list_group_name,
            {
                'type': 'filter_list',
                'message': content
            }
        )

    # for skill filtering
    def filter_list(self, event):
        skill = event['message']['skill']
        skill_obj = TechnicalSkill.objects.get(name=skill)
        prospect = Prospect.objects.filter(technical_skills=skill_obj)
        serializer = ProspectSerializer(prospect, many=True)
        self.send_json(content=serializer.data)

    def prospect_experience(self, event):
        pass

    def prospect_education(self, event):
        pass

    def prospect_technical_skills(self, event):
        pass

    def prospect_contacts(self, event):
        pass

    def prospect_detail(self):
        pass
