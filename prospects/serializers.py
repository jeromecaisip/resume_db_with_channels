from rest_framework import serializers

from .models import Prospect, TechnicalSkill


class ExperienceSerialzer(serializers.Serializer):
    company = serializers.CharField()
    start_date = serializers.DateField()
    end_date = serializers.DateField()


class EducationSerializer(serializers.Serializer):
    school = serializers.CharField()
    start_year = serializers.IntegerField()
    end_year = serializers.IntegerField()

class ContactSerializer(serializers.Serializer):
    mobile_number = serializers.CharField()
    address = serializers.CharField()
    email = serializers.EmailField()


class ProspectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Prospect
        fields = ['id', 'first_name', 'last_name', 'middle_name', 'technical_skills', 'experiences','education','contacts']

    first_name = serializers.CharField(source='personal_information.first_name')
    last_name = serializers.CharField(source='personal_information.last_name')
    middle_name = serializers.CharField(source='personal_information.middle_name')
    technical_skills = serializers.StringRelatedField(read_only=True, many=True)
    experiences = ExperienceSerialzer(many=True)
    education = EducationSerializer(many=True)
    contacts = ContactSerializer(source='personal_information.contacts',many=True)



class TechnicalSkillSerializer(serializers.ModelSerializer):
    class Meta:
        model = TechnicalSkill
        fields = ['name']
