from django.shortcuts import get_object_or_404
from django.shortcuts import render

from .models import Prospect


def index(request):
    prospects = Prospect.objects.all()

    return render(request, 'prospects/index.html', {'prospects': prospects})


def detail(request, prospect_id):
    prospect = get_object_or_404(Prospect, id=prospect_id)
    professional_panels = ['Career Objective',
                           'Education',
                           'Technical Proficiencies',
                           'Experience',
                           'Skills']

    personal_fields = prospect.personal_information._meta.fields

    return render(request, 'prospects/detail.html',
                  {'professional_panels': professional_panels, 'personal_fields': personal_fields,
                   'prospect': prospect})
