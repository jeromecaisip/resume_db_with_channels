from django.urls import reverse
from djongo import models


class Contact(models.Model):
    address = models.CharField(max_length=100)
    mobile_number = models.CharField(max_length=10)
    email = models.EmailField()

    class Meta:
        abstract = True


class Interest(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        abstract = True


class Hobby(models.Model):
    name = models.CharField(max_length=100, )

    class Meta:
        abstract = True


class PersonalInformation(models.Model):
    GENDER_CHOICES = (
        (1, 'Male'),
        (2, 'Female')
    )
    first_name = models.CharField(max_length=100, verbose_name='First Name')
    middle_name = models.CharField(max_length=100, verbose_name='Middle Name')
    last_name = models.CharField(max_length=100, verbose_name='Last Name')
    date_of_birth = models.DateField(verbose_name='Birthday')
    gender = models.IntegerField(choices=GENDER_CHOICES, verbose_name='Gender')
    contacts = models.ArrayModelField(model_container=Contact, verbose_name='Contact Details')
    interests = models.ArrayModelField(model_container=Interest, verbose_name='Interests')
    hobbies = models.ArrayModelField(model_container=Hobby, verbose_name='Hobbies')

    class Meta:
        abstract = True


class CareerObjective(models.Model):
    text = models.TextField()

    class Meta:
        abstract = True


class Education(models.Model):
    school = models.TextField()
    start_year = models.IntegerField()
    end_year = models.IntegerField()

    class Meta:
        abstract = True


class SoftSkills(models.Model):
    name = models.CharField(max_length=100)

    class Meta:
        abstract = True


class TechnicalSkill(models.Model):
    name = models.CharField(max_length=100)
    objects = models.DjongoManager()


    def __str__(self):
        return self.name


class Experience(models.Model):
    company = models.CharField(max_length=100)
    position = models.CharField(max_length=100)
    start_date = models.DateField()
    end_date = models.DateField()

    class Meta:
        abstract = True


class Prospect(models.Model):
    personal_information = models.EmbeddedModelField(model_container=PersonalInformation)
    technical_skills = models.ArrayReferenceField(TechnicalSkill)
    career_objective = models.EmbeddedModelField(model_container=CareerObjective)
    education = models.ArrayModelField(model_container=Education)
    soft_skills = models.ArrayModelField(model_container=TechnicalSkill)
    experiences = models.ArrayModelField(model_container=Experience)

    objects = models.DjongoManager()

    def __str__(self):
        return "{0}, {1} {2}.".format(self.personal_information.last_name, self.personal_information.first_name,
                                      self.personal_information.middle_name[:1])

    def get_absolute_url(self):
        return reverse('prospects:detail', kwargs={'prospect_id': self.id})
