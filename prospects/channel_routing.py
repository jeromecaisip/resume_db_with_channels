from django.urls import path

from . import consumers

urlpatterns = [
    path('ws/prospects/', consumers.ProspectConsumer),
    path('ws/prospects/<int:prospect_id>/', consumers.ProspectConsumer),
]
