from django.urls import path

from . import views

app_name = 'prospects'

urlpatterns = [
    path('', views.index, name='index'),
    path('<int:prospect_id>/', views.detail, name='detail')
]
