## Example Socket Requests

Get previous experiences of a prospect

`webSocketBridge.stream(prospect_id).send({action: 'filter', type: 'experiences'})`

Get educational background of a prospect

`webSocketBridge.stream(prospect_id).send({action: 'filter', type: 'education'})`

Get technical skills of a prospect

`webSocketBridge.stream(prospect_id).send({action: 'filter', type: 'technical_skills'})`

Get contact details of a prospect

`webSocketBridge.stream(prospect_id).send({action: 'filter', type: 'contacts'})`

Get all details about a prospect

`webSocketBridge.stream(prospect_id).send({action: 'retrieve'})`

List of prospects filtered by skill:
`webSocketBridge.send({skill:<str:tech_kill_name>)`